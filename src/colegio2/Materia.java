package colegio2;


import java.util.ArrayList;

public class Materia {
	
	String nombre ;
	boolean esPromocionable;
	ArrayList<Alumno> alumnosInscriptos ;
	
	
	public Materia(String nombre, boolean esPromocionable) {
		if(nombre.length()<3 || nombre.length()>10 ) {
			 new RuntimeException("nombre de alumno invalido ");
		}
		this.nombre = nombre;
		this.esPromocionable = esPromocionable;
		this.alumnosInscriptos=new ArrayList<Alumno>();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isEsPromocionable() {
		return esPromocionable;
	}

	public void setEsPromocionable(boolean esPromocionable) {
		this.esPromocionable = esPromocionable;
	}

	public ArrayList<Alumno> getAlumnosInscriptos() {
		return alumnosInscriptos;
	}

	public void setAlumnosInscriptos(ArrayList<Alumno> alumnosInscriptos) {
		this.alumnosInscriptos = alumnosInscriptos;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toString() {
		return "Materia [nombre=" + nombre + ", esPromocionable=" + esPromocionable + ", alumnosInscriptos="
				+ alumnosInscriptos + "]";
	}

}
