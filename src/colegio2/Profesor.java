package colegio2;


import java.util.ArrayList;

public class Profesor {
	String nombre;
	int nroLegajo ;
	ArrayList<Materia> materiasDictadas;
	
	
	public Profesor(String nombre, int nroLegajo) {
		if(nombre.length()<3 || nombre.length()>10 || nroLegajo<100 ) {
			 new RuntimeException("nombre de profe invalido, o nro de legajo invalido");
		}
		this.nombre=nombre;
		this.nroLegajo=nroLegajo;
		this.materiasDictadas=new ArrayList<Materia>();
		
	}


	@Override
	public String toString() {
		return "Profesor [nombre=" + nombre + ", nroLegajo=" + nroLegajo + ", materiasDictadas=" + materiasDictadas
				+ "]";
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getNroLegajo() {
		return nroLegajo;
	}


	public void setNroLegajo(int nroLegajo) {
		this.nroLegajo = nroLegajo;
	}


	public ArrayList<Materia> getMateriasDictadas() {
		return materiasDictadas;
	}


	public void setMateriasDictadas(ArrayList<Materia> materiasDictadas) {
		this.materiasDictadas = materiasDictadas;
	}


	
}
