package colegio2;


import java.util.ArrayList;

public class Carrera {
	String nombre;
	ArrayList<Materia>  materiasCarrera ;
	ArrayList<Alumno> alumnosInscriptos ;

	
	public Carrera(String nombre) {
		if(nombre.length()<3 || nombre.length()>10  ) {
			 new RuntimeException("nombre de materia invalido");
		}
		this.nombre=nombre;
		this.materiasCarrera=new ArrayList<Materia>();
		this.alumnosInscriptos=new ArrayList<Alumno>();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Materia> getMateriasCarrera() {
		return materiasCarrera;
	}

	public void setMateriasCarrera(ArrayList<Materia> materia) {
		this.materiasCarrera = materia;
	}

	public ArrayList<Alumno> getAlumnosInscriptos() {
		return alumnosInscriptos;
	}

	public void setAlumnosInscriptos(ArrayList<Alumno> alumnosInscriptos) {
		this.alumnosInscriptos = alumnosInscriptos;
	}

	@Override
	public String toString() {
		return "Carrera [nombre=" + nombre + ", materiasCarrera=" + materiasCarrera + ", alumnosInscriptos="
				+ alumnosInscriptos + "]";
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Carrera carrera= new Carrera("licenciado en jesus");
		carrera.setAlumnosInscriptos(null);
		carrera.setMateriasCarrera(null);
	}

}
