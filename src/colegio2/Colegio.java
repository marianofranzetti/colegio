package colegio2;


import java.util.ArrayList;

public class Colegio {
	String nombre;
	String direccion;
	ArrayList<Profesor> profesores;
	ArrayList<Alumno> alumnos;
	ArrayList<Carrera> carreras;

	public Colegio(String nombre, String direccion,ArrayList<Profesor> profesores,
			ArrayList<Alumno> alumnos, ArrayList<Carrera> carreras ) {
		this.nombre=nombre;
		this.direccion=direccion;
		this.profesores=profesores;
		this.alumnos=alumnos;
		this.carreras=carreras;
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Carrera carrera = new Carrera("medicina");
		Carrera carrera2= new Carrera("filosofia");
		ArrayList<Carrera> carrerasDelColegio = new ArrayList<Carrera>();
		carrerasDelColegio.add(carrera);
		carrerasDelColegio.add(carrera2);
		
		Materia materia= new Materia("matematica", false);
		Materia materia2= new Materia("filosofia", true);
		
		Alumno alumno = new Alumno("juan", "pereyra", 40290786);
		Alumno alumno2= new Alumno("jose", "tevez", 40290787);
		ArrayList<Alumno> alumnosDelColegio= new ArrayList<Alumno>();
		alumnosDelColegio.add(alumno);
		alumnosDelColegio.add(alumno2);
		
		Profesor profesor= new Profesor("leo", 123);
		Profesor profesor2= new Profesor("ricardo", 1234);
		ArrayList<Profesor> profesoresDelColegio = new ArrayList<Profesor>();
		profesoresDelColegio.add(profesor);
		profesoresDelColegio.add(profesor2);
		
		
		Colegio colegio = new Colegio("Lourdes", "caballito 123", profesoresDelColegio,
				alumnosDelColegio, carrerasDelColegio);
		
		profesor.materiasDictadas.add(materia);
		//profesor2.carrerasDictadas.add(carrera);
		
		//alumno.materiasInscripto.add(materia);
//		alumno2.materiasInscripto.add(materia2);
		
		//alumno.carrerasInscripto.add(carrera);
		//alumno2.carrerasInscripto.add(carrera2);
		
		materia.alumnosInscriptos.add(alumno);
		//materia2.alumnosInscriptos.add(alumno2);
		
	//	carrera.alumnosInscriptos.add(alumno);
		//carrera2.alumnosInscriptos.add(alumno2);
	//	carrera.alumnosInscriptos.add(alumno2);
		System.out.println(carrera.toString());
		System.out.println(" ");
		System.out.println(materia.toString());
		System.out.println(" ");
		System.out.println(alumno.toString());
		System.out.println(" ");
		System.out.println(profesor.toString());
		System.out.println(" ");
		
		
		System.out.println(colegio.toString());
	}
	@Override
	public String toString() {
		return "Colegio [nombre=" + nombre + ", direccion=" + direccion + ", profesores=" + profesores + ", alumnos="
				+ alumnos + ", carreras=" + carreras + "]";
	}

}
