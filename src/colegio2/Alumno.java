package colegio2;


import java.util.ArrayList;


public class Alumno {
	
	@Override
	public String toString() {
		return "Alumno [nombre=" + nombre + ", apellido=" + apellido + ", dni=" + dni + ", materiasInscripto="
				+ materiasInscripto + ", carrerasInscripto=" + carrerasInscripto + "]";
	}



	String nombre;
	String apellido;
	int dni;
	ArrayList<Materia> materiasInscripto ;
	ArrayList<Carrera> carrerasInscripto ;



	public Alumno(String nombre, String apellido, int dni) {
		if(nombre.length()<3 || nombre.length()>10 || dni<1000000 || 
				apellido.length()<3 || apellido.length()>10 ) {
			 new RuntimeException("nombre de alumno invalido, o apelido invalido o dni ");
		}
		this.nombre=nombre;
		this.apellido=apellido;
		this.dni=dni;
		this.materiasInscripto= new ArrayList<Materia>();
		this.carrerasInscripto= new ArrayList<Carrera>() ;
	}
	
	
	
	public int getDni() {
		return dni;
	}



	public void setDni(int dni) {
		this.dni = dni;
	}



	public ArrayList<Carrera> getCarrerasInscripto() {
		return carrerasInscripto;
	}



	public void setCarrerasInscripto(ArrayList<Carrera> carrera) {
		this.carrerasInscripto = carrera;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getApellido() {
		return apellido;
	}



	public void setApellido(String apellido) {
		this.apellido = apellido;
	}



	public ArrayList<Materia> getMateriasInscripto() {
		return materiasInscripto;
	}



	public void setMateriasInscripto(ArrayList<Materia> materiasInscripto) {
		this.materiasInscripto = materiasInscripto;
	}



	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}


}